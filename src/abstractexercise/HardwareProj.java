/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abstractexercise;

/**
 *
 * @author aluno.redes
 */
public class HardwareProj extends Project{
    private double componentsCost;//all the cost of hardware components is here    
    
    public HardwareProj(String name, double labourHour) {
        super(name, labourHour);
    }

    @Override
    public double currentCost() {//returns the actual cost of the HARDWARE project (workedHours * labourHour + componentsCost)
        return this.hours * this.labourHour + this.componentsCost;
    }

    @Override
    public String info() {
        return "name: "+this.name+"\ncost per worked hour: "+this.labourHour+"\nworked hours: "+this.hours+"\nactual cost: "+this.currentCost();
    }    

    public double getComponentsCost() {
        return componentsCost;
    }

    public void setComponentsCost(double componentsCost) {
        this.componentsCost = componentsCost;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLabourHour() {
        return labourHour;
    }

    public void setLabourHour(double labourHour) {
        this.labourHour = labourHour;
    }

    public double getHours() {
        return hours;
    }

    public void setHours(double hours) {
        this.hours = hours;
    }
    
    
}
