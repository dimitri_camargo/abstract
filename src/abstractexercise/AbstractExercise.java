/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abstractexercise;

import java.util.Scanner;

/**
 *
 * @author aluno.redes
 */
public class AbstractExercise {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        
        
        Project v[] = new Project[3];
        for (int i = 0; i < v.length; i++) {
            System.out.println("\nwrite 'hardware' or 'software' to create the "+(i+1)+"° project: ");
            String option = sc.next().toLowerCase();
            
            System.out.println("write the name of the project: ");
            String name = sc.next();
            
            System.out.println("write the cost of work per hour");
            double labourHour = sc.nextDouble();

            System.out.println("write the worked hours: ");
            double hours = sc.nextDouble();
            
            switch (option) {
                case "hardware"://user choosed to create a hardware project
                    HardwareProj hardProj = new HardwareProj(name, labourHour);
                    //he setted the total hours of the project
                    hardProj.setHours(hours);
                    //the cost of the components
                    System.out.println("write the components cost: ");
                    hardProj.setComponentsCost(sc.nextDouble());
                    //putting the object in the array
                    v[i] = hardProj;
                    break;
                case "software"://user choosed to create a software project
                    SoftwareProj softProj = new SoftwareProj(name, labourHour);
                    softProj.setHours(hours);
                    //he setted the cost of software license
                    System.out.println("write the software license cost: ");
                    softProj.setLicensesCost(sc.nextDouble());
                    v[i] = softProj;
                    break;
                default:
                    System.out.println("some information most be mistaken");
                    throw new AssertionError();
            }            
        }
        
        for (int i = 0; i < v.length; i++) {
            System.out.println("\n"+v[i].info());
        }
        

    }
    
}
