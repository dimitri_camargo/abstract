/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abstractexercise;

/**
 *
 * @author aluno.redes
 */
public class SoftwareProj extends Project{
    private double licensesCost;//the cost with software licenses goes here
    
    public SoftwareProj(String name, double labourHour) {
        super(name, labourHour);
    }    
    
    @Override
    public double currentCost() {
        return this.hours * this.labourHour + this.licensesCost;
    }

    @Override
    public String info() {
        return "name: "+this.name+"\ncost per worked hour"+this.labourHour+"\nworked hours: "+this.hours+"\nactual project cost: "+this.currentCost();
    }    

    public double getLicensesCost() {
        return licensesCost;
    }

    public void setLicensesCost(double licensesCost) {
        this.licensesCost = licensesCost;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLabourHour() {
        return labourHour;
    }

    public void setLabourHour(double labourHour) {
        this.labourHour = labourHour;
    }

    public double getHours() {
        return hours;
    }

    public void setHours(double hours) {
        this.hours = hours;
    }
    
    
}
