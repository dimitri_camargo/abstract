/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abstractexercise;

/**
 *
 * @author aluno.redes
 */
public abstract class Project {
    protected String name;
    protected double labourHour;
    protected double hours = 0;//all the time spent on the project
    
    public Project(String name, double labourHour){
        this.name = name;
        this.labourHour = labourHour;
    }
    
    public void addHours(double hours){
        this.hours+=hours;
    }
    
    public abstract double currentCost();
    
    public abstract String info();
    
}
